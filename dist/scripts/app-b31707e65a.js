(function(){
	  'use strict';

angular.module("renalApp",['ngMaterial','ui.router',  'ui.grid', 'ui.grid.selection', 'ngSanitize', 'ui.bootstrap', 'ui.grid.autoResize', 'ui.grid.resizeColumns','toggle-switch', 'moment-picker']);
})();
(function(){
	 'use strict';

	angular.module("renalApp").service('SaveServices',["commonApi", function(commonApi){
		this.savePatient=function(vm){
			let addresObj=[];
			let len=0;
			if(vm.address.isPrimaryAddress){
				len=1;
			}else{
				len=2;
			}

			for(let i=0;i<len;i++){
				let obj={};
				if(i==1){
					obj=vm.currentAdrs;
				}else{
					obj=vm.address;
				}

			addresObj.push({
					  "patientAddressId": undefined,
					  "patientProfileId": undefined,
					  "isPrimaryAddress": obj.isPrimaryAddress,
					  "location": obj.location,
					  "cityId": obj.cityId,
					  "mandalId": obj.mandalId,
					  "districtId": obj.districtId,
					  "stateProvinceId": obj.stateProvinceId,
					  "pinCode": obj.pinCode,
					  "isActive": true
					})
			}
			let saveObj={
						  "addresses": addresObj,
						  "hospitalProfileId": vm.obj.hositalDetails[0].hospitalProfileId,
						  "hositalDetails":vm.obj.hositalDetails[0],/* {
						    "hospitalProfileId": 0,
						    "name": "string",
						    "location": "string",
						    "cityId": 0,
						    "cityName": "string",
						    "stateProvinceId": 0,
						    "stateProvinceName": "string"
						  }*/
						 // "patientProfileId": 0,
						 // "patientId": "string",
						  "patientName": vm.obj.patientName,
						  //"identityNumber": "string",
						  "relationshipName": vm.obj.relationshipName,
						  "relationshipTypeId": vm.obj.relationshipTypeId,
						  "dateOfBirth": vm.obj.dob,//"2018-02-04T18:45:36.472Z",
						  "gender": vm.obj.gender,
						  "educationId": vm.obj.educationId,
						  "religionId": vm.obj.religionsId,
						  "familyIncomeId": vm.obj.familyIncomeId,
						  "fundingSourceId": vm.obj.fundingSourceId,
						  "isActive": true,
						  "registrationDate": moment(),//"2018-02-04T18:45:36.472Z"
						};

			commonApi.commonApiReq('POST','User/PatientRegistration',saveObj).then(function(res){
						if(res.data!=undefined&&res.data!=""){
							vm.clear();
						}
					},function(err){
						console.log(err);
					})
		}
	}]);
})();
(function(){

	  'use strict';

	patRegCtrl.$inject = ["$scope", "commonApi", "SaveServices"];
	angular.module("renalApp").controller('patRegCtrl',patRegCtrl);

	function patRegCtrl($scope,commonApi,SaveServices){
		var vm=this;
		vm.obj={};
		vm.genData={};
		vm.genDataC={};
		vm.error={};
		vm.errorC={};

		commonApi.commonApiReq('GET','master/GeneralData').then(function(res){
			if(res.data!=undefined&&res.data!=""){
				vm.genData=res.data;
				vm.genDataC.states=res.data.states;
			}
		},function(err){
			console.log(err);
		})

		commonApi.commonApiReq('GET','master/HospitalProfile?hospitalProfileId='+0).then(function(res){
			if(res.data!=undefined&&res.data!=""){
				vm.obj.hositalDetails=res.data;
			}
		},function(err){
			console.log(err);
		})

		//get Age Cal
		vm.changedDate=function(dob){
			vm.obj.age=moment().diff(dob, 'years',false);
		}

		vm.changedAge=function(age){
			if(age!=undefined&&age!=""){
			vm.obj.dob=moment().subtract(parseInt(age), 'years');
			}
		}


		//Change State  get district
		vm.changeValueData=function(stateProvinceId,districtId,mandalId,val,flag){
			let urlVal="";
			if(stateProvinceId!=undefined&&stateProvinceId!=""&&districtId!=undefined&&districtId!=""&&mandalId!=undefined&&mandalId!=""){
				urlVal='master/GetCityByStateDisctrictMandal?stateId='+stateProvinceId+'&districtId='+districtId+'&mandalId='+mandalId;
			}else if(stateProvinceId!=undefined&&stateProvinceId!=""&&districtId!=undefined&&districtId!=""){
				urlVal='master/GetMandalsByStateDistrict?stateId='+stateProvinceId+'&districtId='+districtId;
			}else{
				urlVal='master/GetDistrictsByStateId?stateId='+stateProvinceId;
			}

			commonApi.commonApiReq('GET',urlVal).then(function(res){
						if(res.data!=undefined&&res.data!=""){
							if(flag){
								vm.genDataC[val]=res.data;
							}else{
							vm.genData[val]=res.data;
							}
						}
					},function(err){
						console.log(err);
					})
		}

		vm.validAddress=function(address,error){
			if(address.location==undefined||address.location===""){
				error.location=true;
				vm.flag=true;
			}
			if(address.stateProvinceId==undefined||address.stateProvinceId===""){
				error.stateProvinceId=true;
				vm.flag=true;
			}
			if(address.districtId==undefined||address.districtId===""){
				error.districtId=true;
				vm.flag=true;
			}
			if(address.mandalId==undefined||address.mandalId===""){
				error.mandalId=true;
				vm.flag=true;
			}
			if(address.cityId==undefined||address.cityId===""){
				error.cityId=true;
				vm.flag=true;
			}
			if(address.pinCode==undefined||address.pinCode===""){
				error.pinCode=true;
				vm.flag=true;
			}
		}

		vm.savePatient=function(){
			if(vm.obj.patientName==undefined||vm.obj.patientName===""){
				vm.error.patientName=true;
				vm.flag=true;
			}
			if(vm.obj.relationshipName==undefined||vm.obj.relationshipName===""){
				vm.error.relationshipName=true;
				vm.flag=true;
			}
			if(vm.obj.age==undefined||vm.obj.age===""){
				vm.error.age=true;
				vm.flag=true;
			}
			if(vm.obj.religionsId==undefined||vm.obj.religionsId===""){
				vm.error.religionsId=true;
				vm.flag=true;
			}
			if(vm.obj.educationId==undefined||vm.obj.educationId===""){
				vm.error.educationId=true;
				vm.flag=true;
			}
			if(vm.obj.familyIncomeId==undefined||vm.obj.familyIncomeId===""){
				vm.error.familyIncomeId=true;
				vm.flag=true;
			}
			if(vm.obj.fundingSourceId==undefined||vm.obj.fundingSourceId===""){
				vm.error.fundingSourceId=true;
				vm.flag=true;
			}
			vm.validAddress(vm.address,vm.error);

			if(vm.address!=undefined&&!vm.address.isPrimaryAddress){
				vm.validAddress(vm.currentAdrs,vm.errorC);
			}

				if(!vm.flag){
				SaveServices.savePatient(vm);
				}
		}

		vm.clear=function(){
			vm.obj={};
			vm.address={};
			vm.address.isPrimaryAddress=true;
			vm.currentAdrs={};
			vm.error={};
			vm.obj.relationshipTypeId=1;
			vm.obj.gender='M';
		}
		
	}

})();
(function(){
	 'use strict';

	angular.module("renalApp").service('commonApi',["$http", function($http){
		this.commonApiReq=function(method,url,params){
			 return $http({
					method:method,
					url:'http://123.176.44.158:8020/api/'+url,
					headers: {'Content-Type': 'application/json'},
					params:params
				})
		}
	}]);
})();
(function(){

	  'use strict';

	designCtrl.$inject = ["$scope", "$http"];
	angular.module("renalApp").controller('designCtrl',designCtrl);

	function designCtrl($scope,$http){
		  $scope.fname = "Imran";

  $scope.users = [
    {id: 1,name: 'Bob'},
    {id: 2,name: 'Alice'},
    {id: 3,name: 'Steve'},
    {id: 4,name: 'Nina'},
    {id: 5,name: 'Robert'},
    {id: 6,name: 'Peter'},
    {id: 7,name: 'Bob'},
    {id: 8,name: 'Alice'},
    {id: 9,name: 'Steve'},
    {id: 10,name: 'Nina'},
    {id: 11,name: 'Robert'},
    {id: 12,name: 'Peter'},
    ];
	
	$scope.changedDate=function(){
		$scope.age=new Date().getYear()-new Date(momentTo).getYear();
	}

    $scope.gridOptions = {
        enableRowSelection: true, 
        enableRowHeaderSelection: false,
        enableHorizontalScrollbar: 0,
        enableVerticalScrollbar: 2,
        multiSelect: false
    };

    $scope.gridOptions.columnDefs = [{
            field: "patientUniqueId",
            displayName: "Patient Unique Id"
        }, {
            field: "name",
            displayName: "Name"
        }, {
            field: "age",
            displayName: "Age"
        }, {
            field: "gender",
            displayName: "Gender"
        }, {
            field: "consultingDr",
            displayName: "Consulting Doctor"
        }, {
            field: "cityVillage",
            displayName: "City & Village"
        }, {
            field: "regDateTime",
            displayName: "Registration Date & Time"
        }, {
            field: "actions",
            displayName: "Actions",
            cellTemplate:'<div class="ui-grid-cell-contents renal-action-icons"><a href="">View</a><i class="fa fa-pencil"></i><i class="fa fa-plus"></i><i class="fa fa-trash-o"></i><i class="fa fa-edit"></i></div>'
        }
    ];

    $scope.load1 = function() {
            $http.get('src/data.json').success(function(data) {
                $scope.gridOptions.data = data;
            });
        }

    $scope.load1();

  //   $scope.open = function () {
  //     $modal.open({
  //         templateUrl: 'myModalContent.html',
  //         backdrop: true
  //       });
  // };

  $scope.openModal = function () {
  $uibModal.open({
    templateUrl: 'partials/modal.html',
    windowClass: 'renal-patient-registration-styles',
    size: 'lg',
    controller: ["$scope", "$uibModalInstance", function ($scope, $uibModalInstance) {
      $scope.ok = function () {
        $uibModalInstance.close();
      };
    }]
  })
};


	}



})();
(function(){

	  'use strict';

	//module declartion
	angular.module("renalApp").config(["$stateProvider", "$urlRouterProvider", function($stateProvider,$urlRouterProvider) {

$urlRouterProvider.otherwise('/home');
    $stateProvider
    .state('home',{
    	url:'/home',
        templateUrl : 'app/home.screen.html',
        controller:'mainCtrl',
        controllerAs:'vm'
    })
    .state('home.patReg',{
    	url:'/patReg',
        templateUrl : 'app/consumer/patientReg/partials/renal.patient.registration.html',
        controller:'patRegCtrl',
        controllerAs:'vm'
    })
    .state('home.design',{
        url:'/design',
        templateUrl : 'app/consumer/patientReg/partials/renal.elements.html',
        controller:'designCtrl',
        controllerAs:'vm'
    })

  }]);
})();
(function(){

	  'use strict';

	mainCtrl.$inject = ["$scope", "commonApi", "$state"];
	angular.module("renalApp").controller('mainCtrl',mainCtrl);

	function mainCtrl($scope,commonApi,$state){
		var vm=this;

//
		
/*
$http({
	method:'GET',
	url:'http://123.176.44.158:8020/api/master/GeneralData',
}).then(function(resp) {
		console.log(resp);
	});*/
		//for routing to left menu realted page
		/*main.changePage=function(arg){
			if(arg=='patReg'){
			$state.go('home.patReg');
			}else{
				$state.go('home');
			}
		}*/

	}



})();
angular.module("renalApp").run(["$templateCache", function($templateCache) {$templateCache.put("app/home.screen.html","<div class=renal-container><div class=renal-top-menu-section><div class=logo-holder><img src=app/assets/logo.png></div><div class=renal-hospital-select-section><div class=form-group><i class=\"fa fa-home\" aria-hidden=true></i><div class=dropdown><span class=dropdown-toggle type=button data-toggle=dropdown>Renal Nandyal <span class=caret></span></span><ul class=dropdown-menu><li>Renal Hyderabad</li><li>Renal Mumbai</li><li>Renal Banglore</li></ul></div></div></div><div class=renal-patient-search-section><div class=form-group><div class=\"input-group renal-patient-global-search\"><span class=input-group-addon><i class=\"fa fa-search\"></i> </span><input type=text class=form-control placeholder=\"Find a patient name, id...\"></div></div></div><div class=renal-notification-section><i class=\"fa fa-bell-o\" aria-hidden=true></i></div><div class=renal-doctor-selection-section><img src=app/assets/doctor.jpg><div class=dropdown><span class=dropdown-toggle type=button data-toggle=dropdown>Dr. Rahul Dravid <span class=caret></span></span><ul class=dropdown-menu><li>Dr. Zaheer Khan</li><li>Dr. VVS Laxman</li><li>Dr. Smith</li></ul></div></div></div><div class=renal-side-menu-section><ul class=\"nav nav-pills nav-stacked\"><li class=active ng-click=\"homeD=true;patReg=false;\"><a data-toggle=tab ui-sref=.design><i class=\"fa fa-home\" aria-hidden=true></i></a></li><li ng-click=\"homeD=false;patReg=false;\"><a data-toggle=tab><i class=\"fa fa-bar-chart\" aria-hidden=true></i></a></li><li ng-click=\"patReg=true;homeD=false;\"><a data-toggle=tab ui-sref=.patReg><i class=\"fa fa-user-plus\" aria-hidden=true></i></a></li><li ng-click=\"homeD=false;patReg=false;\"><a data-toggle=tab><i class=\"fa fa-file-text-o\" aria-hidden=true></i></a></li></ul></div><div class=renal-content-container><div class=tab-content><div ui-view></div></div></div></div>");
$templateCache.put("app/consumer/patientReg/partials/modal.html","<div class=renal-container><div class=renal-patient-registration-styles><div class=row><div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><div class=\"form-group pat-search-form-group\"><label>&nbsp;</label><br><div class=\"input-group renal-patient-global-search\"><span class=input-group-addon><i class=\"fa fa-search\"></i> </span><input type=text class=form-control placeholder=\"Find a patient name, id...\"></div></div></div></div><div class=patient-info><div class=row><div class=\"col-xs-10 col-sm-10 col-md-10 col-lg-10\"><span class=patient-name>Mr. Ramesh Kumar -</span> <span class=patient-gender>Male, 29 yrs</span></div><div class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2\"><span class=last-visit-info>2 Days Ago</span></div></div><div class=row><div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><span class=patient-id>APRR2356NEF</span></div></div><div class=clear-fix></div></div><div class=patient-info><div class=row><div class=\"col-xs-10 col-sm-10 col-md-10 col-lg-10\"><span class=patient-name>Mr. Ramesh Kumar -</span> <span class=patient-gender>Male, 29 yrs</span></div><div class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2\"><span class=last-visit-info>2 Days Ago</span></div></div><div class=row><div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><span class=patient-id>APRR2356NEF</span></div></div><div class=clear-fix></div></div><div class=patient-info><div class=row><div class=\"col-xs-10 col-sm-10 col-md-10 col-lg-10\"><span class=patient-name>Mr. Ramesh Kumar -</span> <span class=patient-gender>Male, 29 yrs</span></div><div class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2\"><span class=last-visit-info>2 Days Ago</span></div></div><div class=row><div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><span class=patient-id>APRR2356NEF</span></div></div><div class=clear-fix></div></div><div class=patient-info><div class=row><div class=\"col-xs-10 col-sm-10 col-md-10 col-lg-10\"><span class=patient-name>Mr. Ramesh Kumar -</span> <span class=patient-gender>Male, 29 yrs</span></div><div class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2\"><span class=last-visit-info>2 Days Ago</span></div></div><div class=row><div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><span class=patient-id>APRR2356NEF</span></div></div><div class=clear-fix></div></div><div class=patient-info><div class=row><div class=\"col-xs-10 col-sm-10 col-md-10 col-lg-10\"><span class=patient-name>Mr. Ramesh Kumar -</span> <span class=patient-gender>Male, 29 yrs</span></div><div class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2\"><span class=last-visit-info>2 Days Ago</span></div></div><div class=row><div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><span class=patient-id>APRR2356NEF</span></div></div><div class=clear-fix></div></div><div class=patient-info><div class=row><div class=\"col-xs-10 col-sm-10 col-md-10 col-lg-10\"><span class=patient-name>Mr. Ramesh Kumar -</span> <span class=patient-gender>Male, 29 yrs</span></div><div class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2\"><span class=last-visit-info>2 Days Ago</span></div></div><div class=row><div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><span class=patient-id>APRR2356NEF</span></div></div><div class=clear-fix></div></div></div></div>");
$templateCache.put("app/consumer/patientReg/partials/renal.elements.html","<div class=white-bg><br><button class=renal-info-btn ng-click=openModal()>Modal</button> <button class=renal-success-btn>Save</button> <button class=renal-default-btn>Save</button> <button class=renal-warning-btn>Save</button> <button class=renal-info-btn disabled>Save</button> <button class=renal-success-btn disabled>Save</button> <button class=renal-default-btn disabled>Save</button> <button class=renal-warning-btn disabled>Save</button><br><br><div class=row><div class=\"col-xs-6 col-sm-6 col-md-3 col-lg-3\"><div class=form-group><label class=req-field>First Name</label><br><input type=text class=form-control ng-model=fname placeholder=Enter></div></div><div class=\"col-xs-6 col-sm-6 col-md-3 col-lg-3\"><div class=form-group><label>Middle Name</label><br><input type=text class=form-control placeholder=Enter readonly></div></div><div class=\"col-xs-6 col-sm-6 col-md-3 col-lg-3\"><div class=form-group><label>Last Name</label><br><input type=text class=form-control placeholder=Enter></div></div><div class=\"col-xs-6 col-sm-6 col-md-3 col-lg-3\"><div class=form-group><label>Gender</label><br><input type=text class=\"form-control renal-invalid\" placeholder=Enter></div></div></div><div class=row><div class=\"col-xs-6 col-sm-6 col-md-3 col-lg-3\"><div class=form-group><label>Select Dropdown</label><br><select class=form-control><option>Select One</option><option>Select One</option><option>Select One</option><option>Select One</option><option>Select One</option></select></div></div><div class=\"col-xs-6 col-sm-6 col-md-3 col-lg-3\"><div class=\"form-group renal-multiselect\"><label>Multi Select</label><br><md-select ng-model=selectedUser multiple class=\"md-no-underline form-control\" md-container-class=renal-multiselect><md-option ng-value=user ng-repeat=\"user in users\">{{ user.name }}</md-option></md-select></div></div><div class=\"col-xs-6 col-sm-6 col-md-3 col-lg-3\"><div class=form-group><label>Input Group Addon</label><br><div class=input-group><input type=text class=form-control placeholder=Enter> <span class=input-group-addon><i class=\"fa fa-calendar\"></i></span></div></div></div><div class=\"col-xs-6 col-sm-6 col-md-3 col-lg-3\"><div class=form-group><label>Input Group Addon</label><br><div class=input-group><input type=text class=form-control placeholder=Enter> <span class=input-group-addon><i class=\"fa fa-clock-o\"></i></span></div></div></div></div><div class=row><div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\"><div class=renal-radio-pair><label><input type=checkbox checked>Label</label><label><input type=checkbox>Label</label><label><input type=checkbox disabled>Label</label><label><input type=checkbox checked disabled>Label</label></div><br><div class=renal-radio-pair><label><input type=radio name=optradio checked>Label</label><label><input type=radio name=optradio>Label</label><label><input type=radio disabled>Label</label><label><input type=radio disabled checked>Label</label></div></div><div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\"><div class=renal-switch><toggle-switch ng-model=switchStatus></toggle-switch></div></div></div><div class=row><div class=\"col-xs-3 col-sm-3 col-md-3 col-lg-3\"><div class=\"form-group renal-datepicker\"><label>Select Date</label><div class=input-group><input class=form-control placeholder=\"Select a date...\" moment-picker=stringTo format=LL start-view=month start-date=stringFrom ng-model=momentTo ng-model-options=\"{ updateOn: \'blur\' }\"> <span class=input-group-addon moment-picker=stringTo format=LL start-view=month start-date=stringFrom ng-model-options=\"{ updateOn: \'blur\' }\"><i class=\"fa fa-calendar\"></i></span></div></div></div><div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\"><div class=form-group><label>&nbsp;</label><br><div class=\"input-group renal-patient-global-search\"><span class=input-group-addon><i class=\"fa fa-search\"></i> </span><input type=text class=form-control placeholder=\"Find a patient name, id...\"></div></div></div></div><div class=row><div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 renal-grid\"><div ui-grid=gridOptions class=grid ui.grid.selection ui-grid-auto-resize ui-grid-resize-columns></div></div></div></div><script type=text/ng-template id=myModalContent.html><div class=\"row\">\r\n    <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">\r\n      <div class=\"renal-radio-pair\">\r\n        <label><input type=\"checkbox\" checked>Label</label>\r\n        <label><input type=\"checkbox\">Label</label>\r\n        <label><input type=\"checkbox\" disabled>Label</label>\r\n        <label><input type=\"checkbox\" checked disabled>Label</label>\r\n      </div>\r\n      <br>\r\n      <div class=\"renal-radio-pair\">\r\n        <label><input type=\"radio\" name=\"optradio\" checked>Label</label>\r\n        <label><input type=\"radio\" name=\"optradio\">Label</label>\r\n        <label><input type=\"radio\" disabled>Label</label>\r\n        <label><input type=\"radio\" disabled checked>Label</label>\r\n      </div>\r\n    </div></script>");
$templateCache.put("app/consumer/patientReg/partials/renal.patient.registration.html","<div class=patient-registration-main><div class=renal-page-heading>patient Registration</div><div class=row><div class=\"col-xs-6 col-sm-6 col-md-3 col-lg-3 stick-right\"><div class=aadhar-section><img src=app/assets/finger-print.png><div class=aadhar-no>Aadhar No</div><div class=form-group><input type=text class=form-control placeholder=\"XXXX - XXXX - XXXX - XXXX\"></div><div class=\"renal-button-group margin-10\"><button class=renal-info-btn>Submit</button></div></div></div><form name=renalForm><div class=\"col-xs-12 col-sm-12 col-md-9 col-lg-9 white-bg section-padding\"><div class=renal-section-heading>personal details</div><div class=row><div class=\"col-xs-6 col-sm-6 col-md-4 col-lg-4\"><div class=form-group><label>Patient Name</label><br><input type=text class=form-control placeholder=Enter name=patientName ng-model=vm.obj.patientName ng-class=\"{\'renal-invalid\':vm.error.patientName}\" ng-change=\"vm.error.patientName=false;vm.flag=false;\"></div></div><div class=\"col-xs-6 col-sm-6 col-md-4 col-lg-4\" ng-init=\"vm.obj.relationshipTypeId=1;\"><div class=form-group><span class=renal-radio-pair><label><input type=radio name=guradianName value=1 ng-model=vm.obj.relationshipTypeId checked>Father Name</label><label><input type=radio name=guradianName value=2 ng-model=vm.obj.relationshipTypeId ng-change=\"vm.obj.relationshipTypeId==2?vm.obj.gender=\'F\':undefined\">Husband\'s Name</label></span><br><input type=text class=form-control placeholder=Enter ng-model=vm.obj.relationshipName ng-class=\"{\'renal-invalid\':vm.error.relationshipName}\" ng-change=\"vm.error.relationshipName=false;vm.flag=false;\"></div></div><div class=\"col-xs-6 col-sm-6 col-md-4 col-lg-4\"><div class=\"form-group renal-datepicker\"><label>Date of Birth</label><div class=input-group><input class=form-control placeholder=\"Select a date...\" moment-picker=stringTo format=DD-MM-YYYY start-view=month start-date=stringFrom ng-model=vm.obj.dob ng-class=\"{\'renal-invalid\':vm.error.age}\" ng-model-options=\"{ updateOn: \'blur\' }\" change=\"vm.error.age=false;vm.flag=false;vm.changedDate(vm.obj.dob)\"> <span class=input-group-addon moment-picker=stringTo format=DD-MM-YYYY start-view=month ng-model=vm.obj.dob start-date=stringFrom ng-model-options=\"{ updateOn: \'blur\' }\" change=\"vm.error.age=false;vm.flag=false;vm.changedDate(vm.obj.dob)\"><i class=\"fa fa-calendar\"></i></span></div></div></div></div><div class=row><div class=\"col-xs-6 col-sm-6 col-md-4 col-lg-4\"><div class=form-group><label>Age</label><br><input type=text class=form-control placeholder=Enter ng-model=vm.obj.age ng-change=\"vm.error.age=false;vm.flag=false;vm.changedAge(vm.obj.age)\" ng-class=\"{\'renal-invalid\':vm.error.age}\"></div></div><div class=\"col-xs-6 col-sm-6 col-md-4 col-lg-4\" ng-init=\"vm.obj.gender=\'M\'\"><div class=form-group><label>Gender</label><br><span class=renal-radio-pair><label><input type=radio name=gender value=M ng-model=vm.obj.gender checked>Male</label><label><input type=radio name=gender value=F ng-model=vm.obj.gender>Female</label><label><input type=radio name=gender value=O ng-model=vm.obj.gender>Others</label></span><br></div></div><div class=\"col-xs-6 col-sm-6 col-md-4 col-lg-4\"><div class=form-group><label>Religion</label><br><select class=form-control ng-model=vm.obj.religionsId ng-options=\"item.religionId as item.name for item in vm.genData.religions\" ng-class=\"{\'renal-invalid\':vm.error.religionsId}\" ng-change=\"vm.error.religionsId=false;vm.flag=false;\"><option selected hidden value=\"\">Select Religion</option></select></div></div></div><div class=row><div class=\"col-xs-6 col-sm-6 col-md-4 col-lg-4\"><div class=form-group><label>Education</label><br><select class=form-control ng-model=vm.obj.educationId ng-options=\"item.educationId as item.name for item in vm.genData.educations\" ng-class=\"{\'renal-invalid\':vm.error.educationId}\" ng-change=\"vm.error.educationId=false;vm.flag=false;\"><option selected hidden value=\"\">Select Education</option></select></div></div><div class=\"col-xs-6 col-sm-6 col-md-4 col-lg-4\"><div class=form-group><label>Family Income</label><br><select class=form-control ng-model=vm.obj.familyIncomeId ng-options=\"item.familyIncomeId as item.name for item in vm.genData.familiyIncomes\" ng-class=\"{\'renal-invalid\':vm.error.familyIncomeId}\" ng-change=\"vm.error.familyIncomeId=false;vm.flag=false;\"><option selected hidden value=\"\">Select Family Income</option></select></div></div><div class=\"col-xs-6 col-sm-6 col-md-4 col-lg-4\"><div class=form-group><label>Source of Funding</label><br><select class=form-control ng-model=vm.obj.fundingSourceId ng-options=\"item.fundingSourceId as item.name for item in vm.genData.fundingSources\" ng-class=\"{\'renal-invalid\':vm.error.fundingSourceId}\" ng-change=\"vm.error.fundingSourceId=false;vm.flag=false;\"><option selected hidden value=\"\">Select Source of Funding</option></select></div></div></div><div class=\"renal-section-heading top-margin\">Contact Information</div><div class=renal-sub-section-heading>Permanent Address</div><div class=row><div class=\"col-xs-6 col-sm-6 col-md-8 col-lg-8\"><div class=\"form-group full-width\"><label>Address</label><br><input type=text class=\"form-control full-width\" placeholder=Enter ng-model=vm.address.location ng-class=\"{\'renal-invalid\':vm.error.location}\" ng-change=\"vm.error.location=false;vm.flag=false;\"></div></div><div class=\"col-xs-6 col-sm-6 col-md-4 col-lg-4\"><div class=form-group><label>State</label><br><select class=form-control ng-model=vm.address.stateProvinceId ng-options=\"item.stateProvinceId as item.name for item in vm.genData.states\" ng-class=\"{\'renal-invalid\':vm.error.stateProvinceId}\" ng-change=\"vm.error.stateProvinceId=false;vm.flag=false;vm.changeValueData(vm.address.stateProvinceId,undefined,undefined,\'districtList\');\"><option selected hidden value=\"\">Select State</option></select></div></div><div class=\"col-xs-6 col-sm-6 col-md-4 col-lg-4\"><div class=form-group><label>District</label><br><select class=form-control ng-model=vm.address.districtId ng-options=\"item.districtId as item.name for item in vm.genData.districtList\" ng-class=\"{\'renal-invalid\':vm.error.districtId}\" ng-change=\"vm.error.districtId=false;vm.flag=false;vm.changeValueData(vm.address.stateProvinceId,vm.address.districtId,undefined,\'mandalList\');\"><option selected hidden value=\"\">Select District</option></select></div></div><div class=\"col-xs-6 col-sm-6 col-md-4 col-lg-4\"><div class=form-group><label>Mandal</label><br><select class=form-control ng-model=vm.address.mandalId ng-options=\"item.mandalId as item.name for item in vm.genData.mandalList\" ng-class=\"{\'renal-invalid\':vm.error.mandalId}\" ng-change=\"vm.error.mandalId=false;vm.flag=false;vm.changeValueData(vm.address.stateProvinceId,vm.address.districtId,vm.address.mandalId,\'cityList\');\"><option selected hidden value=\"\">Select Mandal</option></select></div></div><div class=\"col-xs-6 col-sm-6 col-md-4 col-lg-4\"><div class=form-group><label>City / Village</label><br><select class=form-control ng-model=vm.address.cityId ng-options=\"item.cityId as item.name for item in vm.genData.cityList\" ng-class=\"{\'renal-invalid\':vm.error.cityId}\" ng-change=\"vm.error.cityId=false;vm.flag=false;\"><option selected hidden value=\"\">Select City</option></select></div></div><div class=\"col-xs-6 col-sm-6 col-md-4 col-lg-4\"><div class=form-group><label>Pin Code</label><br><input type=text class=form-control ng-model=vm.address.pinCode placeholder=Enter ng-class=\"{\'renal-invalid\':vm.error.pinCode}\" ng-change=\"vm.error.pinCode=false;vm.flag=false;\"></div></div></div><div class=row><div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\" ng-init=\"vm.address.isPrimaryAddress=true;\"><label><input type=checkbox ng-model=vm.address.isPrimaryAddress checked>Permanent Address is same as Current Address (or)</label><span class=renal-link ng-click=\"vm.address.isPrimaryAddress=false;\">New</span></div></div><div class=\"renal-sub-section-heading top-margin\" ng-if=!vm.address.isPrimaryAddress>Current Address</div><div class=row ng-if=!vm.address.isPrimaryAddress><div class=\"col-xs-6 col-sm-6 col-md-8 col-lg-8\"><div class=\"form-group full-width\"><label>Address</label><br><input type=text class=\"form-control full-width\" placeholder=Enter ng-model=vm.currentAdrs.location ng-class=\"{\'renal-invalid\':vm.errorC.location}\" ng-change=\"vm.errorC.location=false;vm.flag=false;\"></div></div><div class=\"col-xs-6 col-sm-6 col-md-4 col-lg-4\"><div class=form-group><label>State</label><br><select class=form-control ng-model=vm.currentAdrs.stateProvinceId ng-options=\"item.stateProvinceId as item.name for item in vm.genDataC.states\" ng-class=\"{\'renal-invalid\':vm.errorC.stateProvinceId}\" ng-change=\"vm.errorC.stateProvinceId=false;vm.flag=false;vm.changeValueData(vm.currentAdrs.stateProvinceId,undefined,undefined,\'districtList\',true);\"><option selected hidden value=\"\">Select State</option></select></div></div><div class=\"col-xs-6 col-sm-6 col-md-4 col-lg-4\"><div class=form-group><label>District</label><br><select class=form-control ng-model=vm.currentAdrs.districtId ng-options=\"item.districtId as item.name for item in vm.genDataC.districtList\" ng-class=\"{\'renal-invalid\':vm.errorC.districtId}\" ng-change=\"vm.errorC.districtId=false;vm.flag=false;vm.changeValueData(vm.currentAdrs.stateProvinceId,vm.currentAdrs.districtId,undefined,\'mandalList\',true);\"><option selected hidden value=\"\">Select District</option></select></div></div><div class=\"col-xs-6 col-sm-6 col-md-4 col-lg-4\"><div class=form-group><label>Mandal</label><br><select class=form-control ng-model=vm.currentAdrs.mandalId ng-options=\"item.mandalId as item.name for item in vm.genDataC.mandalList\" ng-class=\"{\'renal-invalid\':vm.errorC.mandalId}\" ng-change=\"vm.errorC.mandalId=false;vm.flag=false;vm.changeValueData(vm.currentAdrs.stateProvinceId,vm.currentAdrs.districtId,vm.currentAdrs.mandalId,\'cityList\',true);\"><option selected hidden value=\"\">Select Mandal</option></select></div></div><div class=\"col-xs-6 col-sm-6 col-md-4 col-lg-4\"><div class=form-group><label>City / Village</label><br><select class=form-control ng-model=vm.currentAdrs.cityId ng-options=\"item.cityId as item.name for item in vm.genDataC.cityList\" ng-class=\"{\'renal-invalid\':vm.errorC.cityId}\" ng-change=\"vm.errorC.cityId=false;vm.flag=false;\"><option selected hidden value=\"\">Select City</option></select></div></div><div class=\"col-xs-6 col-sm-6 col-md-4 col-lg-4\"><div class=form-group><label>Pin Code</label><br><input type=text class=form-control ng-model=vm.currentAdrs.pinCode placeholder=Enter ng-class=\"{\'renal-invalid\':vm.errorC.pinCode}\" ng-change=\"vm.errorC.pinCode=false;vm.flag=false;\"></div></div></div><div class=row><div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><div class=\"renal-button-group top-margin\"><button class=renal-default-btn ng-click=vm.clear()>Clear</button> <button class=renal-success-btn ng-click=vm.savePatient()>Save</button></div></div></div></div></form></div></div>");}]);
//# sourceMappingURL=../maps/scripts/app-b31707e65a.js.map
