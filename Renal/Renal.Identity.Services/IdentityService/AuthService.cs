﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Renal.DataAccess.Auth;
using Renal.Identity.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Renal.Identity.Services.IdentityService
{
    public class AuthService : IAuthService
    {

        public IAuthRepository _authRepo;

        public AuthService(IAuthRepository authRepo)
        {
            _authRepo = authRepo;
        }

        public Task<IdentityResult> RegisterUser(UserModel userModel)
        {
            return _authRepo.RegisterUser(userModel);
        }

        public Task<IdentityUser> FindAsync(UserLoginInfo loginInfo)
        {
            return _authRepo.FindAsync(loginInfo);
        }

        public Task<IdentityResult> CreateAsync(IdentityUser user)
        {
            return _authRepo.CreateAsync(user);
        }

        public Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login)
        {
            return _authRepo.AddLoginAsync(userId, login);
        }

        public Client FindClient(string clientId)
        {
            return _authRepo.FindClient(clientId);
        }
    }
}
