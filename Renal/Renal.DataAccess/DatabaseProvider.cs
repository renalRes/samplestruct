﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace Renal.DataAccess
{
    public interface IDatabaseProvider
    {
        IDbConnection GetConnection { get; }
    }

    public class DatabaseProvider: IDatabaseProvider
    {
        public IDbConnection GetConnection
        {
            get
            {
                return new SqlConnection(ConfigurationManager.ConnectionStrings[""].ConnectionString);
            }
        }
    }
}
