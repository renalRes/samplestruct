﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/******
  https://stackoverflow.com/questions/31298235/how-to-implement-unit-of-work-pattern-with-dapper/31636037#31636037
 *****/


namespace Renal.DataAccess
{
    public sealed class UnitOfWorkSession : IDisposable
    {
        public UnitOfWorkSession(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        IDbConnection _connection = null;
        IUnitOfWork _unitOfWork = null;

        public IUnitOfWork UnitOfWork
        {
            get { return _unitOfWork; }
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }
}
