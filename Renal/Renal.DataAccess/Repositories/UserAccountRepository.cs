﻿using Renal.DataAccess.IRepositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Renal.DataAccess.Repositories
{
    internal class UserAccountRepository : RepositoryBase, IUserAccountRepository
    {
        private IDbConnection _connection;
        public UserAccountRepository(IDbTransaction transaction) : base(transaction)
        {
            _connection = transaction.Connection;
        }

        public void GetUserData()
        {
        }
    }
}
