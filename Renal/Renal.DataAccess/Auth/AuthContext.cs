﻿using Microsoft.AspNet.Identity.EntityFramework;
using Renal.DataAccess.Modal.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Renal.DataAccess.Auth
{
    public class AuthContext : IdentityDbContext<IdentityUser>, IAuthContext
    {
        public AuthContext()
            : base("AuthContext")
        {

        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
    }
}
