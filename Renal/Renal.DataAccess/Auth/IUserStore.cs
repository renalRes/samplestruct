﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Renal.DataAccess.Auth
{
    public interface IUserStore : IUserStore<IdentityUser>
    {
    }
}
