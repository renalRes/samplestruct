﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Renal.DataAccess.Auth
{
    public interface IUserManager<TUser, TKey> : IDisposable
    where TUser : class, IUser<TKey>
    where TKey : System.IEquatable<TKey>
    {
        IQueryable<TUser> Users { get; }
        Task<TUser> FindByEmailAsync(string email);
        Task<TUser> FindByIdAsync(TKey userId);
        Task<IdentityResult> CreateAsync(TUser user, string password);
        Task<TUser> FindAsync(string userName, string password);
        Task<TUser> FindAsync(UserLoginInfo login);
        Task<IdentityResult> CreateAsync(TUser user);
        Task<IdentityResult> AddLoginAsync(TKey userId, UserLoginInfo login);
    }

    public interface IUserManager<TUser> : IUserManager<TUser, string> where TUser : class, IUser<string>
    {

    }

    public interface IApplicationManager : IUserManager<IdentityUser>
    {

    }
}
