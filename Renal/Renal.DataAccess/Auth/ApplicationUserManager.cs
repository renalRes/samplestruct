﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Renal.DataAccess.Auth
{
    public class ApplicationUserManager: UserManager<IdentityUser>, IApplicationManager
    {
        public ApplicationUserManager(IUserStore userStore) : base(userStore)
        {

        }
    }
}
